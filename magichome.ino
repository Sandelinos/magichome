#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#define DEVICENAME "YOUR DEVICE NAME"
#define redPin 5
#define greenPin 13
#define bluePin 12

const char* ssid = "WIFI SSID";
const char* password = "WIFI PASSWORD";
const char* mqtt_server = "MQTT SERVER IP ADDRESS";
const char* mqtt_username = "MQTT SERVER USERNAME";
const char* mqtt_password = "MQTT USER PASSWORD";
const char* light_state_topic = "MQTT STATUS TOPIC FOR THIS LIGHT";
const char* light_control_topic = "MQTT CONTROL TOPIC FOR THIS LIGHT";
const char* light_control_topic_all = "MQTT CONTROL TOPIC FOR ALL LIGHTS";
const int mqtt_port = 1883;

int inR;
int inG;
int inB;
char state[11];

WiFiClient espClient;
PubSubClient client(espClient);

void mqttconnect() {
  while(!client.connected()) {
    Serial.println("[MQTT] Connecting...");
    if (client.connect(DEVICENAME, mqtt_username, mqtt_password)) {
      Serial.println("[MQTT] Connected!");
      client.subscribe(light_control_topic);
      Serial.print("[MQTT] Subscribed to ");
      Serial.println(light_control_topic);
      client.subscribe(light_control_topic_all);
      Serial.print("[MQTT] Subscribed to ");
      Serial.println(light_control_topic_all);
    } else {
      Serial.println("[MQTT] failed");
      delay(5000);
    }
  }
}

void setColor(int outR, int outG, int outB) {
  analogWrite(redPin, outR);
  analogWrite(greenPin, outG);
  analogWrite(bluePin, outB);
  Serial.print("[COLOR] Set LEDs: ");
  Serial.print(outR);
  Serial.print(", ");
  Serial.print(outG);
  Serial.print(", ");
  Serial.println(outB);
}

void callback(char* topic, byte* payload, unsigned int length) {
  payload[length] = '\0';
  String value = String((char*)payload);
  inR = value.substring(0,value.indexOf(',')).toInt();
  inG = value.substring(value.indexOf(',')+1,value.lastIndexOf(',')).toInt();
  inB = value.substring(value.lastIndexOf(',')+1).toInt();
  setColor(inR, inG, inB);
  value.toCharArray(state, 11);
  client.publish(light_state_topic, state);
}

void setup() {
  Serial.begin(115200);
  analogWriteRange(250);
  Serial.println();
  Serial.print("[WIFI] Connecting to ");
  Serial.print(ssid);

  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  
  Serial.println();
  Serial.print("[WIFI] Connected! IP address: ");
  Serial.println(WiFi.localIP());

  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);
}

void loop() {
  if (!client.connected()) {
    mqttconnect();
  }
  client.loop();
}
